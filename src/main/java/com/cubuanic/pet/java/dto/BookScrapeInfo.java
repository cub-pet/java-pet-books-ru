package com.cubuanic.pet.java.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class BookScrapeInfo implements Serializable {
    private String coverUrl;
    private String title;
    private List<String> authors;
    private List<String> links;
}
