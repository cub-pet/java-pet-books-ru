package com.cubuanic.pet.java.domain;

import com.cubuanic.pet.java.dto.BookScrapeInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class BookDownloadInfo implements Serializable {
    private BookScrapeInfo bookScrapeInfo;
    private String normalizedTitle;
    private String normalizedAuthor;
    private String fileBase;

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public BookDownloadInfo(BookScrapeInfo bookScrapeInfo) {
        this.bookScrapeInfo = bookScrapeInfo;
        normalizedTitle = normalizeTitle();
        normalizedAuthor = normalizeAuthor();
        fileBase = normalizedTitle
                + (!normalizedAuthor.isEmpty() ? " -- " + normalizedAuthor : "");
    }

    public String getCoverFilename() {
        return fileBase + "." + getFiletypeByExtension(bookScrapeInfo.getCoverUrl());
    }

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public Map<String, String> getBookFilenames() {
        Map<String, String> result = HashMap.newHashMap(2);
        for (String link : bookScrapeInfo.getLinks()) {
            result.put(link, fileBase + "." + getFiletypeParameter(link));
        }
        return result;
    }

    record PatternToReplacementPair(Pattern pattern, String replacement) {
    }

    // order does matter
    private static final List<PatternToReplacementPair> normalizeSteps = List.of(
            new PatternToReplacementPair(Pattern.compile(" \\(файл [a-zA-Z]+\\)"), ""),         // FILE_STRING - remove " (файл PDF)"
            new PatternToReplacementPair(Pattern.compile("—+"), "-"),                           // EM_DASH
            new PatternToReplacementPair(Pattern.compile("/+"), ", "),                          // SLASH_ENUMERATION
            new PatternToReplacementPair(Pattern.compile("[:\\[\\]]+"), " -"),                  // EXTRA_SYMBOLS
            new PatternToReplacementPair(Pattern.compile("[. ]?\\([^()]+\\)$"), ""),            // EXTRA_PARENS
            new PatternToReplacementPair(Pattern.compile("\\.$"), ""),                          // EXTRA_DOT
            new PatternToReplacementPair(Pattern.compile("([^ 0-9])\\.([^ 0-9])"), "$1. $2"),   // SPACE_AFTER_DOT
            new PatternToReplacementPair(Pattern.compile("[!@'\\\\]"), ""),                     // DELETE_SYMBOLS
            new PatternToReplacementPair(Pattern.compile("[.,] том 3"), ", том 3"),             //
            new PatternToReplacementPair(Pattern.compile("[.,] Том II"), ", том 2"),            //
            new PatternToReplacementPair(Pattern.compile("[.,] Том I"), ", том 1"),             //
            new PatternToReplacementPair(Pattern.compile("  +"), " ")                           // DOUBLE_SPACES
    );

    private String normalizeFilenamePart(String filenamePart) {
        for (PatternToReplacementPair normalizeStep : normalizeSteps) {
            Matcher matcher = normalizeStep.pattern.matcher(filenamePart);
            filenamePart = matcher.replaceAll(normalizeStep.replacement);
        }
        return filenamePart;
    }

    private String normalizeAuthor() {
        StringBuilder normalized = new StringBuilder();
        for (String author : bookScrapeInfo.getAuthors()) {
            author = normalizeFilenamePart(author);
            if (!normalized.isEmpty()) {
                normalized.append(", ");
            }
            normalized.append(author);
        }
        return String.valueOf(normalized);
    }


    private String normalizeTitle() {
        return normalizeFilenamePart(bookScrapeInfo.getTitle());
    }

    private static final Pattern FILETYPE_PATTERN = Pattern.compile("file_type%3D([a-zA-Z]+)");

    private String getFiletypeParameter(String url) {
        String defaultFiletype = "pdf";
        Matcher matcher = FILETYPE_PATTERN.matcher(url);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return defaultFiletype;
    }

    private static final Pattern EXTENSION_PATTERN = Pattern.compile("\\.([a-zA-Z]+)$");

    private String getFiletypeByExtension(String url) {
        String defaultFiletype = "jpg";
        Matcher matcher = EXTENSION_PATTERN.matcher(url);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return defaultFiletype;
    }

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public String toString() {
        return getClass().getName()
                + "("
                + "coverUrl=" + bookScrapeInfo.getCoverUrl() + ", "
                + "title=" + bookScrapeInfo.getTitle() + ", "
                + "authors=" + bookScrapeInfo.getAuthors().toString() + ", "
                + "links=" + bookScrapeInfo.getLinks().toString() + ", "
                + "coverFile=" + getCoverFilename() + ", "
                + "filenames=" + getBookFilenames().toString()
                + ")";
    }

    public String getTitle() {
        return bookScrapeInfo.getTitle();
    }

    public String getCoverUrl() {
        return bookScrapeInfo.getCoverUrl();
    }
}
