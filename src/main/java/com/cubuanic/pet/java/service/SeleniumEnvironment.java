package com.cubuanic.pet.java.service;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Service
public class SeleniumEnvironment {
    @Value("${booksru.downloader.selenium.timeout}")
    private String seleniumTimeout;

    private final JavascriptExecutor jse;

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public SeleniumEnvironment(WebDriver webDriver) {
        this.jse = (JavascriptExecutor) webDriver;
    }

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public <T> T executeWithTimeout(Supplier<T> code) {
        try {
            FutureTask<T> timeoutTask = new FutureTask<>(code::get);
            new Thread(timeoutTask).start();
            return timeoutTask.get(Duration.parse(seleniumTimeout).toSeconds(), TimeUnit.SECONDS);
        } catch (Exception e) {
            if (jse != null) {
                jse.executeScript("window.stop();");
            }
        }

        return null;
    }
}
