# Preface

I've bought a lot of books on http://books.ru, but downloading all of them via browser is too boring.

So this project is to automate downloading books from this specific site and to show my practical experience with Spring Boot, Selenium, processes interaction, testing etc.

Login information is NOT obfuscated, so you could run it locally - but please, remove downloaded files and do not distribute them.

# This is simple project to demonstrate following skills

* Spring Boot (command line runner) + Lombok
* Selenium
* Wget
* testing: JUnit4, PMD, CPD
* fetch files (PDF/EPUB) from remote source

# Howto
* build: `mvn clean package`
* open Jacoco report: `open target/jacoco-report/index.html`
* run downloader: `java -jar target/books-ru-1.0-SNAPSHOT.jar`
