package com.cubuanic.pet.java;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class BooksRuDownloaderConfiguration {
    @Value("${booksru.downloader.selenium.timeout}")
    private String seleniumTimeout;

    @Bean
    public WebDriver webDriver() {
//        FirefoxOptions options = new FirefoxOptions();
//        options.setPageLoadStrategy(PageLoadStrategy.EAGER);

        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
//        options.setBinary(new File("/usr/local/bin/chromedriver"));
//        options.addArguments("--disable-downloader");

//        options.addArguments("enable-automation"); // https://stackoverflow.com/a/43840128/1689770
//        options.addArguments("--headless"); // only if you are ACTUALLY running headless
//        options.addArguments("--no-sandbox"); //https://stackoverflow.com/a/50725918/1689770
//        options.addArguments("--disable-infobars"); //https://stackoverflow.com/a/43840128/1689770
//        options.addArguments("--disable-dev-shm-usage"); //https://stackoverflow.com/a/50725918/1689770
//        options.addArguments("--disable-browser-side-navigation"); //https://stackoverflow.com/a/49123152/1689770
//        options.addArguments("--disable-gpu"); //https://stackoverflow.com/questions/51959986/how-to-solve-selenium-chromedriver-timed-out-receiving-message-from-renderer-exc

//        WebDriver driver = new FirefoxDriver(options);

//        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        WebDriver driver = new ChromeDriver(options);

//        driver.manage().timeouts().pageLoadTimeout(
//            2 * seleniumTimeout,
//            TimeUnit.SECONDS
//        );

//        driver.manage().timeouts().pageLoadTimeout(30L, TimeUnit.SECONDS);
//        driver.manage().timeouts().setScriptTimeout(3L, TimeUnit.SECONDS);
        return driver;
    }

//    @Bean
//    public JavascriptExecutor javascriptExecutor(WebDriver driver) {
//        return (JavascriptExecutor) driver;
//    }

    @Bean
    public WebDriverWait webDriverWait(WebDriver driver) {
        return new WebDriverWait(
                driver,
                Duration.parse(seleniumTimeout)
        );
    }
}
