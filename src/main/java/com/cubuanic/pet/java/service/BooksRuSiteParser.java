package com.cubuanic.pet.java.service;

import com.cubuanic.pet.java.dto.BookScrapeInfo;
import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Log
public class BooksRuSiteParser {
    private final SeleniumEnvironment seleniumEnvironment;
    private final WebDriver webDriver;
    private final WebDriverWait webDriverWait;

    @Value("${booksru.downloader.site.login_url}")
    private String siteLoginUrl;

    @Value("${booksru.downloader.site.orders_url}")
    private String siteOrdersUrl;

    @Value("${booksru.downloader.site.user}")
    private String siteUser;

    @Value("${booksru.downloader.site.password}")
    private String sitePassword;


    public BooksRuSiteParser(
            SeleniumEnvironment seleniumEnvironment,
            WebDriver webDriver,
            WebDriverWait webDriverWait) {
        this.seleniumEnvironment = seleniumEnvironment;
        this.webDriver = webDriver;
        this.webDriverWait = webDriverWait;
    }

    public void login() {
        webDriver.get(siteLoginUrl);

        WebElement login = webDriver.findElement(By.name("login"));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(login));
        login.sendKeys(siteUser);

        WebElement password = webDriver.findElement(By.name("password"));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(password));
        password.sendKeys(sitePassword);

        WebElement rememberMe = webDriver.findElement(By.name("rememberme"));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(rememberMe));
        rememberMe.click();

        WebElement loginForm = webDriver.findElement(By.cssSelector("form#loginform"));
        loginForm.submit();

//        selenium.executeWithTimeout(() -> {
//            WebElement loginButton = webDriver.findElement(
//                By.cssSelector("form#loginform .loginbt")
//            );
//            loginButton.click();
//            return null;
//        });

        log.info("Logged in successfully");
    }

    public void quit() {
        webDriver.quit();
    }

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public List<BookScrapeInfo> parseSite() {
        log.info("Site parsing started");
        final List<BookScrapeInfo> booksScrapeInfo = new ArrayList<>(500);

        List<String> ordersUrls = getOrdersUrls();
        for (String url : ordersUrls) {
            final List<BookScrapeInfo> orderBooksInfo = processOrder(url);
            booksScrapeInfo.addAll(orderBooksInfo);
        }

        log.info("Site parsing finished");
        return booksScrapeInfo;
    }

    private List<String> getOrdersUrls() {
        log.info("Orders parsing started");

//        selenium.executeWithTimeout(() -> {
//            webDriver.get(SITE_ORDERS_URL);
//            return null;
//        });

        webDriver.get(siteOrdersUrl);
        List<WebElement> orderLinks = webDriver.findElements(By.cssSelector("table.orders a"));
        List<String> result = new ArrayList<>(10);
        for (WebElement link : orderLinks) {
            result.add(link.getAttribute("href"));
        }

        log.info("Orders parsing finished");
        return result;
    }

    private List<BookScrapeInfo> processOrder(String orderUrl) {
        log.info("Order parsing started: " + orderUrl);

        webDriver.get(orderUrl);

//        selenium.executeWithTimeout(() -> {
//            webDriver.get(orderUrl);
//            return null;
//        });

        List<WebElement> orderItems = webDriver.findElements(By.cssSelector("table.catalog tbody tr:not(.total)"));

        List<BookScrapeInfo> booksScrapeInfo = new ArrayList<>(10);
        for (WebElement orderItem : orderItems) {
            final WebElement img = orderItem.findElement(By.cssSelector("td.cover img"));
            String bookImg = img.getAttribute("src");
            String bookTitle = img.getAttribute("alt");

            List<String> bookAuthors = extractData(
                    orderItem,
                    "td.descr a[data-title]",
                    "text");

            List<String> bookLinks = extractData(
                    orderItem,
                    "td.status a.pdf",
                    "href");

            booksScrapeInfo.add(new BookScrapeInfo(bookImg, bookTitle, bookAuthors, bookLinks));
        }

        log.info("Order parsing finished");
        return booksScrapeInfo;
    }

    private List<String> extractData(
            WebElement orderItem,
            String selector,
            String attribute) {
        List<String> extractedData = new ArrayList<>(3);
        final List<WebElement> elements = orderItem.findElements(By.cssSelector(selector));
        for (WebElement element : elements) {
            extractedData.add(element.getAttribute(attribute));
        }
        return extractedData;
    }
}

