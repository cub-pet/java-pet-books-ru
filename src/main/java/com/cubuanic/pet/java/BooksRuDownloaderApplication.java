package com.cubuanic.pet.java;

import com.cubuanic.pet.java.dto.BookScrapeInfo;
import com.cubuanic.pet.java.service.BooksRuSiteParser;
import com.cubuanic.pet.java.service.ScrapeInfoStorage;
import com.cubuanic.pet.java.service.WgetDownloader;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@Log
@AllArgsConstructor
public class BooksRuDownloaderApplication implements CommandLineRunner {
    private final ScrapeInfoStorage scrapeInfoStorage;
    private final WgetDownloader downloader;
    private final BooksRuSiteParser siteParser;

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public static void main(String[] args) {
        new SpringApplicationBuilder(BooksRuDownloaderApplication.class)
            .bannerMode(Banner.Mode.OFF)
            .run(args);
    }

    @Override
    public void run(String... args) {
        log.info("Starting...");

        siteParser.login();

        final List<BookScrapeInfo> booksScrapeInfo;
        if (scrapeInfoStorage.exists()) {
            log.info("Found "
                    + scrapeInfoStorage.getFilename()
                    + ", reading stored state");
            booksScrapeInfo = scrapeInfoStorage.read();
        } else {
            booksScrapeInfo = siteParser.parseSite();
            log.info("Parsed info for " + booksScrapeInfo.size() + " books");
            if (!booksScrapeInfo.isEmpty()) {
                log.info("Storing state to " + scrapeInfoStorage.getFilename());
                scrapeInfoStorage.store(booksScrapeInfo);
            }
        }

        if (!booksScrapeInfo.isEmpty()) {
            log.info("Found info for " + booksScrapeInfo.size() + " books, starting download");
            downloader.download(scrapeInfoStorage, booksScrapeInfo);
        } else {
            log.warning("Can't find books info, nothing to download");
        }

//        siteParser.quit();
    }
}

