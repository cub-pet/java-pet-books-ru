package com.cubuanic.pet.java.domain;

import com.cubuanic.pet.java.dto.BookScrapeInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


class BookDownloadInfoTest {
    private static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of(
                        // coverUrl
                        "https://files.books.ru/pic/653001-654000/653111/000653111s.jpg",
                        // title
                        "Рефакторинг SQL-приложений (файл PDF)",
                        // list of authors
                        List.of("Стефан Фаро", "Паскаль Лерми"),
                        // list of download links
                        List.of("https://new.books.ru/member/download_agreement"
                                + ".php?back_url=%2Fbooks%2Frefaktoring-sql-prilozhenii-fail-pdf"
                                + "-653111%2Fdownload%2F%3Ffile_type%3Dpdf"),
                        // expected local coverFilename
                        "Рефакторинг SQL-приложений -- Стефан Фаро, Паскаль Лерми.jpg",
                        // expected book download link to local filename pairs
                        Map.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Frefaktoring-sql-prilozhenii-fail-pdf-653111"
                                        + "%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "Рефакторинг SQL-приложений -- Стефан Фаро, Паскаль Лерми.pdf")
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3642001-3643000/3642806/003642806.jpg",
                        "Основы цифровой обработки сигналов: Курс лекций. 2-е изд",
                        List.of(),
                        List.of("https://new.books.ru/member/download_agreement"
                                + ".php?back_url=%2Fbooks%2Fosnovy-tsifrovoi-obrabotki-signalov-kurs"
                                + "-lektsii-2-e-izd-3642806%2Fdownload%2F%3Ffile_type%3Dpdf"),
                        "Основы цифровой обработки сигналов - Курс лекций. 2-е изд.jpg",
                        Map.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fosnovy-tsifrovoi-obrabotki-signalov-kurs-lektsii"
                                        + "-2-e-izd-3642806%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "Основы цифровой обработки сигналов - Курс лекций. 2-е изд.pdf")
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752503/003752503.jpg",
                        "Измерительные устройства на базе микропроцессора Atmega (+ информация на сайте)",
                        List.of(),
                        List.of("https://new.books.ru/member/download_agreement"
                                + ".php?back_url=%2Fbooks%2Fizmeritelnye-ustroistva-na-baze"
                                + "-mikroprotsessora-atmega--informatsiya-na-saite-3752503%2Fdownload"
                                + "%2F%3Ffile_type%3Dpdf"),
                        "Измерительные устройства на базе микропроцессора Atmega.jpg",
                        Map.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fizmeritelnye-ustroistva-na-baze-mikroprotsessora"
                                        + "-atmega--informatsiya-na-saite-3752503%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "Измерительные устройства на базе микропроцессора Atmega.pdf")
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752520/003752520.jpg",
                        "SEO - искусство раскрутки сайтов.(2-е изд.)",
                        List.of(),
                        List.of("https://new.books.ru/member/download_agreement"
                                + ".php?back_url=%2Fbooks%2Fseoiskusstvo-raskrutki-saitov2-e-izd"
                                + "-3752520%2Fdownload%2F%3Ffile_type%3Dpdf"),
                        "SEO - искусство раскрутки сайтов.jpg",
                        Map.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fseoiskusstvo-raskrutki-saitov2-e-izd-3752520"
                                        + "%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "SEO - искусство раскрутки сайтов.pdf")
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752508/003752508.jpg",
                        "jQuery Mobile: разработка приложений для смартфонов и планшетов",
                        List.of(),
                        List.of("https://new.books.ru/member/download_agreement"
                                + ".php?back_url=%2Fbooks%2Fjquery-mobile-razrabotka-prilozhenii-dlya"
                                + "-smartfonov-i-planshetov-3752508%2Fdownload%2F%3Ffile_type%3Dpdf"),
                        "jQuery Mobile - разработка приложений для смартфонов и планшетов.jpg",
                        Map.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fjquery-mobile-razrabotka-prilozhenii-dlya"
                                        + "-smartfonov-i-planshetov-3752508%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "jQuery Mobile - разработка приложений для смартфонов и планшетов.pdf")
                ),
                Arguments.of(
                        "https://files.books.ru/pic/818001-819000/818144/818144s.jpg",
                        "Do Good Design: как дизайнеры могут изменить мир (файл PDF)",
                        List.of("Дэвид Берман"),
                        List.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fdo-good-design-kak-dizainery-mogut"
                                        + "-izmenit-mir-fail-pdf-818144%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fdo-good-design-kak-dizainery-mogut"
                                        + "-izmenit-mir-fail-pdf-818144%2Fdownload%2F%3Ffile_type%3Depub"),
                        "Do Good Design - как дизайнеры могут изменить мир -- Дэвид Берман.jpg",
                        Map.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fdo-good-design-kak-dizainery-mogut-izmenit-mir"
                                        + "-fail-pdf-818144%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "Do Good Design - как дизайнеры могут изменить мир -- Дэвид Берман.pdf",
                                "https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fdo-good-design-kak-dizainery-mogut-izmenit-mir"
                                        + "-fail-pdf-818144%2Fdownload%2F%3Ffile_type%3Depub",
                                "Do Good Design - как дизайнеры могут изменить мир -- Дэвид Берман.epub")
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Программирование микроконтроллерных плат Arduino/Freeduino",
                        List.of(),
                        List.of("https://new.books.ru/member/download_agreement"
                                + ".php?back_url=%2Fbooks%2Fprogrammirovanie-mikrokontrollernykh-plat"
                                + "-arduinofreeduino-3752502%2Fdownload%2F%3Ffile_type%3Dpdf"),
                        "Программирование микроконтроллерных плат Arduino, Freeduino.jpg",
                        Map.of("https://new.books.ru/member/download_agreement"
                                        + ".php?back_url=%2Fbooks%2Fprogrammirovanie-mikrokontrollernykh-plat"
                                        + "-arduinofreeduino-3752502%2Fdownload%2F%3Ffile_type%3Dpdf",
                                "Программирование микроконтроллерных плат Arduino, Freeduino.pdf")
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Электротехника : учебник для вузов. 3-е изд",
                        List.of(),
                        List.of(),
                        "Электротехника - учебник для вузов. 3-е изд.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Возрождение бренда: шесть принципов. Вдохните в свой бренд новую жизнь вместе с "
                                + "McDonald\\'s. (файл PDF)",
                        List.of(),
                        List.of(),
                        "Возрождение бренда - шесть принципов. Вдохните в свой бренд новую жизнь вместе с McDonalds.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Протокол SIP.Справочник по телекоммуникационным протоколам",
                        List.of(),
                        List.of(),
                        "Протокол SIP. Справочник по телекоммуникационным протоколам.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Photoshop CS4 — это просто. Экспресс-методы обработки фотографий",
                        List.of(),
                        List.of(),
                        "Photoshop CS4 - это просто. Экспресс-методы обработки фотографий.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Веб-дизайн - книга Стива Круга или \"не заставляйте меня думать!\", 2-е издание",
                        List.of("Стив Круг"),
                        List.of(),
                        "Веб-дизайн - книга Стива Круга или \"не заставляйте меня думать\", 2-е издание -- Стив Круг.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Курс высшей математики, том 3, часть 1",
                        List.of(),
                        List.of(),
                        "Курс высшей математики, том 3, часть 1.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Курс высшей математики. Том I",
                        List.of(),
                        List.of(),
                        "Курс высшей математики, том 1.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Курс высшей математики. Том II",
                        List.of(),
                        List.of(),
                        "Курс высшей математики, том 2.jpg",
                        null
                ),
                Arguments.of(
                        "https://files.books.ru/pic/3752001-3753000/3752502/003752502.jpg",
                        "Qt4.8. Профессиональное программирование на C++",
                        List.of(),
                        List.of(),
                        "Qt4.8. Профессиональное программирование на C++.jpg",
                        null
                )
        );
    }


    @ParameterizedTest
    @MethodSource("data")
    void checkCoverFilename(
            String coverUrl,
            String title,
            List<String> authors,
            List<String> links,
            String expectedCoverFilename,
            Map<String, String> expectedBookFilenames
    ) {
        BookDownloadInfo bookDownloadInfo = new BookDownloadInfo(new BookScrapeInfo(coverUrl, title, authors, links));
        assertThat(bookDownloadInfo.getCoverFilename(), is(expectedCoverFilename));
        if (expectedBookFilenames != null) {
            assertThat(bookDownloadInfo.getBookFilenames(), is(expectedBookFilenames));
        }
    }
}