package com.cubuanic.pet.java.service;

import com.cubuanic.pet.java.domain.BookDownloadInfo;
import com.cubuanic.pet.java.dto.BookScrapeInfo;
import lombok.extern.java.Log;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

@Service
@Log
public class WgetDownloader {
    @Value("${booksru.downloader.download.base_dir}")
    private String downloadBaseDir;

    private final WebDriver webDriver;
    private String wgetCmd;

    public WgetDownloader(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private String getWgetCmd() {
        if (wgetCmd != null) {
            return wgetCmd;
        }

        wgetCmd = "wget -q"
                + "     --cookies=on"
                + "     --header 'accept: "
                + String.join(";",
                    "text/html,application/xhtml+xml,application/xml",
                    "q=0.9,image/webp,image/apng,*/*",
                    "q=0.8,application/signed-exchange",
                    "v=b3", "q=0.9"
                 ) + "'"
                + "     --header 'accept-encoding: gzip, deflate, br'"
                + "     --header 'accept-language: en-US,en;q=0.9'"
                + "     --header 'sec-fetch-mode: navigate'"
                + "     --header 'sec-fetch-site: none'"
                + "     --header 'upgrade-insecure-requests: 1'"
                + "     --header 'user-agent: + "
                + String.join(" ",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1)"
                        + "AppleWebKit/537.36 (KHTML, like Gecko)"
                        + "Chrome/79.0.3945.130 Safari/537.36")
                + "'"
                + "     --header 'Cookie: " + prepareCookie() + "'";
        return wgetCmd;
    }

    private String prepareCookie() {
        final Set<Cookie> seleniumCookies = webDriver.manage().getCookies();

        final StringBuilder cookieHeader = new StringBuilder();
        for (Iterator<Cookie> iterator = seleniumCookies.iterator(); iterator.hasNext(); ) {
            Cookie cookie = iterator.next();
            cookieHeader
                    .append(cookie.getName())
                    .append("=")
                    .append(cookie.getValue());

            if (iterator.hasNext()) {
                cookieHeader.append("; ");
            }
        }
        return String.valueOf(cookieHeader);
    }

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public void download(ScrapeInfoStorage scrapeInfoStorage,
                         List<BookScrapeInfo> booksInfo) {
        for (BookScrapeInfo bookScrapeInfo : booksInfo) {
            BookDownloadInfo bookDownloadInfo = new BookDownloadInfo(bookScrapeInfo);
            log.info("Downloading " + bookDownloadInfo.getTitle());
            try {
                downloadCover(getWgetCmd(), bookDownloadInfo);
                downloadBooks(getWgetCmd(), bookDownloadInfo);
                scrapeInfoStorage.store(booksInfo);
            } catch (RuntimeException e) {
                log.info("Error downloading " + bookDownloadInfo.getTitle());
                log.info(bookDownloadInfo.toString());
                e.printStackTrace();
            }
        }
    }

    private String getDownloadFilename(String filename) {
        return downloadBaseDir + filename;
    }

    private boolean isFileDownloaded(String filename) {
        if (new File(getDownloadFilename(filename)).exists()) {
            log.info("Skipping " + filename + ", already downloaded");
            return true;
        }
        return false;
    }

    private void downloadCover(String wgetCmd, BookDownloadInfo bookDownloadInfo) {
        final String coverFilename = bookDownloadInfo.getCoverFilename();
        if (isFileDownloaded(coverFilename)) {
            return;
        }
        final String url = bookDownloadInfo.getCoverUrl();
        executeCommand(wgetCmd, url, coverFilename);
    }

    private void downloadBooks(String wgetCmd, BookDownloadInfo bookDownloadInfo) {
        for (Map.Entry<String, String> filename : bookDownloadInfo.getBookFilenames().entrySet()) {
            final String ebookFilename = filename.getValue();
            if (isFileDownloaded(ebookFilename)) {
                continue;
            }
            final String url = getDirectDownloadUrl(filename.getKey());
            executeCommand(wgetCmd, url, ebookFilename);
        }
    }

    private void executeCommand(String wgetCmd, String url, String output) {
        final String commandLine = wgetCmd
                + " '" + url + "'"
                + " -O '" + getDownloadFilename(output) + "'";
        try {
            Process process = Runtime.getRuntime().exec(
                    new String[]{"/bin/sh", "-c", commandLine},
                    new String[]{"PATH=/bin:/usr/bin:/usr/local/bin"}
            );
            process.waitFor();
            int exitValue = process.exitValue();
            if (exitValue != 0) {
                Scanner scanner = new Scanner(process.getErrorStream());
                while (scanner.hasNext()) {
                    log.info(scanner.nextLine());
                }
                throw new RuntimeException("Failed with exit code " + exitValue);
            }
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getDirectDownloadUrl(String link) {
        try {
            URL url = new URI(link).toURL();
            List<NameValuePair> queryParams = URLEncodedUtils.parse(
                    url.getQuery(),
                    Charset.defaultCharset()
            );

            String backUrl = queryParams
                    .stream()
                    .filter(pair -> pair.getName().equals("back_url"))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Can't detect back_url"))
                    .getValue();

            return url.getProtocol() + "://" + url.getHost() + backUrl;
        } catch (MalformedURLException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}

