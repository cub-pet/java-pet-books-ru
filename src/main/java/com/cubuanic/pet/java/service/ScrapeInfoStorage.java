package com.cubuanic.pet.java.service;

import com.cubuanic.pet.java.dto.BookScrapeInfo;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;

@Service
public class ScrapeInfoStorage {
    private static final String BOOKS_INFO_FILENAME = "books-ru-scrape.info";
    private static final File booksInfoFile = new File(BOOKS_INFO_FILENAME);

    public String getFilename() {
        return BOOKS_INFO_FILENAME;
    }

    public boolean exists() {
        return booksInfoFile.exists();
    }

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public List<BookScrapeInfo> read() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(booksInfoFile));
            return (List<BookScrapeInfo>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    @SuppressWarnings("checkstyle:MissingJavadocMethod")
    public void store(List<BookScrapeInfo> booksScrapeInfo) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(booksInfoFile));
            oos.writeObject(booksScrapeInfo);
            oos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
